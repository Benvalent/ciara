import React, { useState } from "react";
import { Tab, Nav, Button, Modal } from "react-bootstrap";
import Conversations from "./Conversations";
import Contacts from "./Contacts";
import NewContactModal from "./modals/NewContactModal";
import NewConversationModal from "./modals/NewConversationModal";
import { faPlaceOfWorship } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CONVERSATIONS_KEY = "conversations";
const CONTACTS_KEY = "contacts";

export default function Sidebar({ id }) {
  const [activeKey, setActiveKey] = useState(CONVERSATIONS_KEY);
  const [modalOpen, setModalOpen] = useState(false);
  const [menuOpen, setMenuOpen] = useState(false);
  const conversationsOpen = activeKey === CONVERSATIONS_KEY;

  function openModal() {
    setModalOpen(true);
  }

  function closeModal() {
    setModalOpen(false);
  }

  function toggleMenu() {
    setMenuOpen(!menuOpen);
  }

  return (
    <div className="d-flex flex-column sidebar">
      <div className="user-name-header text-light p-2 mt-0 mt-md-2 mb-0 mb-md-3 ml-2 d-flex justify-content-between align-items-center">
        <h2 className="d-flex align-items-center mb-0 mb-md-2">
          <FontAwesomeIcon icon={faPlaceOfWorship} className="mr-3" size="xs" />
          Ciara
        </h2>
        <button
          id="hamburger-icon"
          onClick={toggleMenu}
          className={`mr-2 mt-2 ${menuOpen ? "open" : ""}`}
        >
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </button>
      </div>
      <div
        className={`sidebar-content ${
          menuOpen ? "sidebar-content-open" : "sidebar-content-closed"
        }`}
      >
        <Tab.Container activeKey={activeKey} onSelect={setActiveKey}>
          <Nav variant="tabs">
            <Nav.Item>
              <Nav.Link eventKey={CONVERSATIONS_KEY}>Conversations</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey={CONTACTS_KEY}>Contacts</Nav.Link>
            </Nav.Item>
          </Nav>
          <Tab.Content className="overflow-auto flex-grow-1">
            <Tab.Pane eventKey={CONVERSATIONS_KEY}>
              <Conversations />
            </Tab.Pane>
            <Tab.Pane eventKey={CONTACTS_KEY}>
              <Contacts />
            </Tab.Pane>
          </Tab.Content>
          <div className="p-2 text-muted">
            <p>
              Your ID: <br />
              <span>{id}</span>
            </p>
          </div>
          <Button onClick={openModal} className="btn-rounded mx-3 mb-3 p-2">
            New {conversationsOpen ? "conversation" : "contact"}
          </Button>
        </Tab.Container>
      </div>
      <Modal show={modalOpen} onHide={closeModal}>
        {conversationsOpen ? (
          <NewConversationModal closeModal={closeModal} />
        ) : (
          <NewContactModal closeModal={closeModal} />
        )}
      </Modal>
    </div>
  );
}
