const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
const socketIo = require("socket.io");
const PORTS = {
  BACKEND: process.env.PORT || 5000,
  FRONTEND: process.env.PORT_FRONTEND || 3000,
};

const app = express();

app.use((req, res, next) => {
  console.log(`Request_Endpoint: ${req.method} ${req.url}`);
  next();
});

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());

app.use(express.static(path.join(__dirname, "client/build")));

app.get("*", function (req, res) {
  res.sendFile(path.join(__dirname, "client/build/index.html"));
});

const http = require("http").createServer(app);
const io = socketIo(http);

io.on("connection", (socket) => {
  // use static ID
  const id = socket.handshake.query.id;
  socket.join(id);

  socket.on("send-message", ({ recipients, text }) => {
    recipients.forEach((recipient) => {
      const newRecipients = recipients.filter((r) => r !== recipient);
      newRecipients.push(id);
      socket.broadcast.to(recipient).emit("receive-message", {
        recipients: newRecipients,
        sender: id,
        text,
      });
    });
  });
});

http.listen(PORTS.BACKEND, () => {
  console.log("listening on port", PORTS.BACKEND);
});
